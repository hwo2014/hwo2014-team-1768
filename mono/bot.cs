using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;

public class Bot {
	List<int> bendIndexes;
	GameInit gameInit;
	string myCarName;
	int myCarIndex;
	bool crashed = false;
	bool turboUsable = false;

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
		string raceType = "createRace";
		int carCount = 1;
		string trackName = "keimola";
		string password = "";
		try {
			raceType = args[4]; //createRace or joinRace
			carCount = int.Parse(args[5]);
			trackName = args[6];
			password = args[7];
		} catch {}
		
		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;
			
			if(args.Length > 4) {
				if(raceType == "createRace") {
					new Bot (reader, writer, new CreateRace(botName, botKey, trackName, password, carCount));
				} else {
					new Bot (reader, writer, new JoinRace(botName, botKey, trackName, password, carCount));
				}	
			} else {
				new Bot(reader, writer, new Join(botName, botKey));
			}
		}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, SendMsg joinMessage) { //era Join
		this.writer = writer;
		string line;
		bendIndexes = new List<int>();
		int myTick = 0;
		//double traveledPieceCoefficient = 0;
		double speed = 0;
		double lastInPiecePosition = 0;
		int lastSwitchChange = -1;
		
		send(joinMessage);

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
					CarPosition[] carPositions = JsonConvert.DeserializeObject<CarPosition[]>(msg.data.ToString());
					int currentPieceIndex = carPositions[myCarIndex].piecePosition.pieceIndex;
					int currentLane = carPositions[myCarIndex].piecePosition.lane.startLaneIndex;
					//traveledPieceCoefficient = carPositions[myCarIndex].piecePosition.inPieceDistance / gameInit.race.track.pieces[currentPieceIndex].getLength(currentLane);
					if (carPositions[myCarIndex].piecePosition.inPieceDistance - lastInPiecePosition > 0) {
						speed = carPositions[myCarIndex].piecePosition.inPieceDistance - lastInPiecePosition;
					}
					lastInPiecePosition = carPositions[myCarIndex].piecePosition.inPieceDistance;
					
					//switch lane code
					if (gameInit.race.track.pieces[currentPieceIndex].isSwitch() && (lastSwitchChange != currentPieceIndex)) {
						if(carPositions[myCarIndex].piecePosition.lane.startLaneIndex == carPositions[myCarIndex].piecePosition.lane.endLaneIndex) {
							int bestLane = getShortestLane(currentPieceIndex);
							int laneDiff = bestLane - currentLane;
							
							if (laneDiff != 0) {
								if(laneDiff > 0) {
									send(new SwitchLane("Right"));
								} else {
									send(new SwitchLane("Left"));
								}
								
								Console.WriteLine(String.Format("Change lane to {0}", laneDiff));
								lastSwitchChange = currentPieceIndex;
							}
						}
					}
				
					//turbo code
					if (turboUsable) {
						turboUsable = false;
						
						if ((carPositions[myCarIndex].piecePosition.lap == 2) && (currentPieceIndex == gameInit.race.track.pieces.Length - 2)) {
							Console.WriteLine("Using turbo");
							send(new Turbo());
						}
					}
				
					//throtle code
					double baseSpeed = 0.6;

					double earlyStartBonus = 0;
					
					if(carPositions[myCarIndex].piecePosition.pieceIndex == 0 && carPositions[myCarIndex].piecePosition.lap <= 0) {
						earlyStartBonus = 0.5;
					}
					
					double mixedBendAngleBonus = 0;
					
					//if not the last piece
					if(currentPieceIndex != gameInit.race.track.pieces.Length - 1) {
						if (gameInit.race.track.pieces[currentPieceIndex].angle * gameInit.race.track.pieces[currentPieceIndex + 1].angle < 0) {
							mixedBendAngleBonus = -0.25;
						}
					}
					
					double noBendsBonus = 0;
					
					if (!pieceAtIndexIsBend(currentPieceIndex)) {
						if(!pieceAtIndexIsBend(currentPieceIndex + 1)) {
							if (!pieceAtIndexIsBend(currentPieceIndex + 2)) {
								if(!pieceAtIndexIsBend(currentPieceIndex + 3)) {
									noBendsBonus = 0.22;
								} else {
									noBendsBonus = 0.12;
								}
							} else {
								noBendsBonus = 0.08;
							}
						}
					}
					
					double bendBrake = 0;
					
					if(pieceAtIndexIsBend(currentPieceIndex)) {
						Piece currentPiece = gameInit.race.track.pieces[currentPieceIndex];
						if(speed > 3) { 
							if(currentPiece.angle == Math.Abs(45)) {
								if (currentPiece.radius == 50) {
									bendBrake = -0.4;
								} else if(currentPiece.radius == 100) {
									if (Math.Abs(carPositions[myCarIndex].angle) < 3) {
										bendBrake = 0;
									} else {
										bendBrake = -0.18;
									}
								}
							}
						}
					}
					
					double nextIsBendBrake = 0;
					
					if(!pieceAtIndexIsBend(currentPieceIndex)) {
						if (pieceAtIndexIsBend(currentPieceIndex + 1)) {
						Piece nextPiece = gameInit.race.track.pieces[currentPieceIndex + 1];
							
							if (speed > 3){
								if (nextPiece.radius == 50) {
									nextIsBendBrake = -0.55;
								} else {
									nextIsBendBrake = -0.28;
								}
							}
						}
					}
					
					double angle = Math.Abs(carPositions[myCarIndex].angle);
					double driftBonus = 0;
					driftBonus = 0.1 - Math.Abs((angle - 30) / 30 * 0.1);
					
					double roadRageBonus = 0;
					
					if (!pieceAtIndexIsBend(currentPieceIndex) && !pieceAtIndexIsBend(currentPieceIndex + 1)) {
						bool carNext = false;
						
						for(int i= 0; i< carPositions.Length; i++) {
							if(carPositions[i].piecePosition.pieceIndex <= currentPieceIndex + 1) {
								if(carPositions[i].id.name != myCarName) {
									carNext = true;
									break;
								}
							}
						}
						if (carNext) {
							roadRageBonus = 0.2;
						}
					}
					
					double throttle = Math.Max(0, Math.Min(1, 
					baseSpeed + 
					earlyStartBonus + 
					driftBonus + 
					mixedBendAngleBonus + 
					noBendsBonus + 
					bendBrake + 
					nextIsBendBrake + 
					roadRageBonus
					));
					
					if (!crashed){
						if (myTick % 4 == 0) {
							Console.WriteLine(String.Format("Piece: {0}, speed: {1}, throttle: {2}, angle: {3}",
							currentPieceIndex,
							speed.ToString("n2"),
							throttle.ToString("n2"),
							carPositions[myCarIndex].angle.ToString("n2")));
						}
					} else {
						Console.Write(".");
					}
					
					myTick++;
					send(new Throttle(throttle));
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;			
				case "yourCar":
					YourCar yourCar = JsonConvert.DeserializeObject<YourCar>(msg.data.ToString());
					myCarName = yourCar.name;
					send(new Ping());
					break;
				case "gameInit":
					gameInit = JsonConvert.DeserializeObject<GameInit>(msg.data.ToString());
					Console.WriteLine(gameInit.race.ToString());
					
					for(int i = 0; i < gameInit.race.cars.Length; i++) {
						if (gameInit.race.cars[i].id.name == myCarName) {
							myCarIndex = i;
							Console.WriteLine("Race init, myCarIndex is " + i);
							break;
						}
					}
					
					bendIndexes = gameInit.race.track.getBendIndexes();
										
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				case "crash": {
						Id id = JsonConvert.DeserializeObject<Id>(msg.data.ToString());
						if(id.name == myCarName) {
							Console.WriteLine("Your car crashed, waiting for spawn");
							crashed = true;
						}
						send(new Ping());
					}
					break;
				case "spawn": {
						Id id = JsonConvert.DeserializeObject<Id>(msg.data.ToString());
						if(id.name == myCarName) {
							Console.WriteLine("\n\rYou car is ready!");
							crashed = false;
						}
						send(new Ping());
					}
					break;
				case "turboAvailable":
					TurboAvailable turboAvailable = JsonConvert.DeserializeObject<TurboAvailable>(msg.data.ToString());
					turboUsable = true;
					if (!crashed) {
						Console.WriteLine(turboAvailable.ToString());
					} else {
						Console.WriteLine("\n\r" + turboAvailable.ToString());
					}
					send(new Ping());
					break;
				case "lapFinished":
					Console.WriteLine("Someone completed a lap");
					send(new Ping());
					break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
	
	private bool pieceAtIndexIsBend(int index) {
		return bendIndexes.Contains(index);
	}
	
	public int getShortestLane(int startingPiece) {
		Piece[] pieces = gameInit.race.track.pieces;
		int lanesAmount = gameInit.race.track.lanes.Length;
		double[] lanesDist = new double[lanesAmount];
		int currentPiece = startingPiece;
		
		do {
			for(int i = 0; i < lanesAmount; i++) {
				lanesDist[i] += pieces[currentPiece].getLength(gameInit.race.track.lanes[i].distanceFromCenter);
			}
			
			currentPiece++;
			if (currentPiece == pieces.Length) {
				currentPiece = 0;
			}
		} while(!pieces[currentPiece].isSwitch());
		
		double minValue = lanesDist.Min();
		int shortestLane = Array.IndexOf(lanesDist, minValue);
		
		return shortestLane;
	}
}

#region message receiver classes

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

class YourCar {
	public string name = "unnamed";
	public string color = "colorless";
	
	public override string ToString() {
		return String.Format("Name: {0}, Color: {1}", name, color);
	}
}

class GameInit {
	public Race race = null;
}

class Race {
	public Track track = null;
	public Car[] cars = null;
	public RaceSession raceSession = null;
	
	public override string ToString() {
		string output = "";
		output += "[RACE]\n\r";
		output += track.ToString() + "\n\r";
		output += "[CARS]\n\r" ;
		
		int i = 0;
		
		for (i = 0; i < cars.Length; i++) {
			output += String.Format("[{0}]{1}\n\r", i, cars[i].ToString());
		}
		
		output += raceSession.ToString();
		return output;
	}
}

class Track {
	public string id = "";
	public string name = "";
	public Piece[] pieces = null;
	public Lane[] lanes = null;
	public StartingPoint startingPoint = null;
	
	public List<int> getBendIndexes() {
		List<int> bendIndexes = new List<int>();
		
		for (int i = 0; i < pieces.Length; i++){
			if (pieces[i].isBend()){
				bendIndexes.Add(i);
			}
		}
		return bendIndexes;
	}
	
	public override string ToString() {
		string output= "";
		output += String.Format("[TRACK] id: {0}, name: {1}\n\r", id, name);
		output += "[PIECES]\n\r";
		
		int i = 0;
		
		for (i = 0; i < pieces.Length; i++) {
			output += String.Format("[{0}]{1}\n\r", i, pieces[i].ToString());
		}
		
		output += "[LANES]\n\r";
		
		for (i = 0; i < lanes.Length; i++) {
			output += String.Format("[{0}]{1}\n\r", i, lanes[i].ToString());
		}
		
		output += startingPoint.ToString();
				
		return output;
	}
}

class Piece {
	public double length = 0;
	public bool @switch = false;
	public double angle = 0;
	public int radius = 0;
	
	public bool isBend() {
		return angle != 0;
	}
	
	public bool isSwitch() {
		return @switch;
	}
	
	public double getLength(int distanceFromCenter) {
		double realLength = 0;
		
		if (length != 0) {
			realLength = length;
		} else {
			realLength = angle / 180 * Math.PI * (radius - distanceFromCenter);
		}
		
		return Math.Abs(realLength);
	}
	
	public override string ToString(){
		return String.Format("[PIECE] length: {0}, switch: {1}, angle: {2}, radius: {3}", length, @switch, angle, radius);
	}
}

class Lane {
	public int distanceFromCenter = 0;
	public int index = -1;
	
	public override string ToString() {
		return String.Format("[LANE] distanceFromCenter: {0}, index: {1}", distanceFromCenter, index);
	}
}

class StartingPoint {
	public Position position = null;
	public double angle = 0;
	
	public override string ToString() {
		return String.Format("[STARTING POINT] position: {0}, angle: {1}", position.ToString(), angle);
	}
}

class Position {
	public double x = 0;
	public double y = 0;
	
	public override string ToString() {
		return String.Format("[POSITION] x: {0}, y: {1}", x, y);
	}
}

class Car {
	public Id id = null;
	public double angle = 0;
	public Dimensions dimensions = null;
	
	public override string ToString() {
		return String.Format("[CAR]\n\r{0}\n\r{1}", id.ToString(), dimensions.ToString());
	}
}

class Id {
	public string name = "nameless";
	public string color = "colorless";
	
	public override string ToString() {
		return String.Format("[ID] name: {0}, color: {1}", name, color);
	}
}

class Dimensions {
	public double length = 0;
	public double width = 0;
	public double guideFlagPosition = 0;
	
	public override string ToString() {
		return String.Format("[DIMENSIONS] length: {0}, width: {1}, guideFlagPosition: {2}", length, width, guideFlagPosition);
	}
}

class RaceSession {
	public int laps = -1;
	public int maxLapTimeMs = -1;
	public bool quickRace = true;
	
	public override string ToString() {
		return String.Format("[RACE SESSION] laps: {0}, maxLapTimeMs: {1}, quickRace: {2}", laps, maxLapTimeMs, quickRace);
	}
}

class CarPosition {
	public Id id = null;
	public double angle = 0;
	public PiecePosition piecePosition = null;
	
	public override string ToString() {
		return String.Format("[CAR POSITION]\n\r{0}\n\rangle: {1}\n\r{2}", id.ToString(), angle, piecePosition.ToString());
	}
}

class PiecePosition {
	public int pieceIndex = -1;
	public double inPieceDistance = 0;
	public LaneSwitch lane;
	public int lap = -1;
	
	public override string ToString() {
		return String.Format("[PIECE POSITION] pieceIndex: {0}, inPieceDistance: {1}\n\r{2}\n\rlap: {3}",
		pieceIndex, inPieceDistance, lane.ToString(), lap);
	}
}

class LaneSwitch {
	public int startLaneIndex = -1;
	public int endLaneIndex = -1;
	
	public override string ToString() {
		return String.Format("[LANE] startLaneIndex: {0}, endLaneIndex: {1}", startLaneIndex, endLaneIndex);
	}
}

class Crash {
	public Id id = null;
	public string gameId = "";
	public int gameTick = 0;
	
	public override string ToString() {
		return String.Format("[CRASH]{0}\n\rgameId: {1}, gameTick: {2}", id, gameId, gameTick);
	}
}

class TurboAvailable {
	public double turboDurationMilliseconds = 0;
	public int turboDurationTicks = 0;
	public double turboFactor = 0;
	
	public override string ToString() {
		return String.Format("[Turbo Available] milis: {0}, ticks: {1}, turboFactor: {2}", turboDurationMilliseconds, turboDurationTicks, turboFactor);
	}
}
#endregion

#region sendMsg classes

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class CreateRace: SendMsg {
	public BotId botId = null;
	public string trackName = "";
	public string password = "";
	public int carCount = 0;
	
	public CreateRace(string name, string key, string trackName, string password, int carCount) {
		botId = new BotId();
		botId.name = name;
		botId.key = key;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	
	protected override Object MsgData() {
		return this;
	}
	
	protected override string MsgType() {
		return "createRace";
	}
}

class BotId {
	public string name = "nameless";
	public string key = "noKey";
}

class JoinRace: SendMsg {
	public BotId botId;
	public string trackName;
	public string password;
	public int carCount;
	
	public JoinRace(string name, string key, string trackName, string password, int carCount) {
		botId = new BotId();
		botId.name = name;
		botId.key = key;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	
	protected override Object MsgData() {
		return this;
	}
	
	protected override string MsgType() {
		return "joinRace";
	}
}

class SwitchLane: SendMsg {
	public string lane;
	
	public SwitchLane(string lane) {
		this.lane = lane;
	}
	
	protected override Object MsgData() {
		return lane;
	}
	
	protected override string MsgType() {
		return "switchLane";
	}
}

class Turbo: SendMsg {
	protected override Object MsgData() {
		return "Turbo!! Yay!!";
	}
	
	protected override string MsgType() {
		return "turbo";
	}
}

#endregion